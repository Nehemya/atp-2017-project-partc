package View;

import Model.MyModel;
import ViewModel.MyViewModel;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.ScrollEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.Optional;

public class Main extends Application {
    private static Stage curStage;
    private static MyViewModel viewModel;
    @Override
    public void start(Stage primaryStage) throws Exception{
        curStage=primaryStage;
        MyModel model = new MyModel();
        viewModel = new MyViewModel(model);
        model.addObserver(viewModel);
        /************************************/
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("MyView.fxml").openStream());
        primaryStage.setTitle("TheMazeRunner");
        Scene scene = new Scene(root, 500, 500);
        scene.getStylesheets().add(getClass().getResource("ViewStyle.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
        /***********************************/
        MyViewController viewController = fxmlLoader.getController();
        viewController.startDraw();
        viewController.setResizeEvent(scene);
        viewController.setChoiceBox();
        viewController.setViewModel(viewModel);
        viewModel.addObserver(viewController);
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(455);
        //SetStageCloseEvent(primaryStage);

        primaryStage.setOnCloseRequest(event -> {
            event.consume();
            closeProgram();
        });
        scene.setOnScroll((ScrollEvent event)->{
            if(event.isControlDown()){double deltaY=event.getDeltaY();
                primaryStage.setWidth(primaryStage.getWidth()+deltaY);
                primaryStage.setHeight(primaryStage.getHeight()+deltaY);}


        });

    }
    public static void closeProgram(){
        viewModel.closeProgram();
        curStage.close();
    }




    public static void main(String[] args) {
        launch(args);
    }
}
