package View;

import ViewModel.MyViewModel;
import algorithms.search.MazeState;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * this is the controller for our view layer.
 */
public class MyViewController implements Observer,IView
{

  @FXML
  public javafx.scene.control.Button btn_getMaze;
  public javafx.scene.control.Button btn_getSolution;
  public javafx.scene.control.TextField txt_Col;
  public javafx.scene.control.TextField txt_Row;
  public javafx.scene.layout.BorderPane mainWin;
  public javafx.scene.control.Label status;
  public javafx.scene.control.ChoiceBox<String> MoveKeys;
  @FXML
  MazeView MazeView;
  private MyViewModel VM;
  private boolean bIsBusy = false, bWin = false;

    /**getter for status bar
     * @return status
     */
  public Label getStatus() {
    return status;
  }

    /**
     * setter for status label
     * @param status
     */
  public void setStatus(Label status) {
    this.status = status;
  }

    /**
     * this func set up the choice box
     */
  public void setChoiceBox(){
      MoveKeys.getItems().add("Arrows");
      MoveKeys.getItems().add("A_W_S_D");
      MoveKeys.getItems().add("4_8_5_6");
      MoveKeys.setValue("Arrows");
  }

    /**
     * this func starts the call for a new maze to be generated
     */
  public void GenMaze() {
    btn_getMaze.setDisable(true);
    btn_getSolution.setDisable(true);
    VM.GenMaze();
    mainWin.requestFocus();
  }

    /**
     * gets the solution for the cur maze and print it on board
     */
  public void getSolution() {

    btn_getSolution.setDisable(true);
    btn_getMaze.setDisable(true);
    VM.GenSolution();
    mainWin.requestFocus();

  }

    /**
     * this func catch key events and move the character equally
     * @param keyEvent
     */
  public void KeyPressed(KeyEvent keyEvent) {//aviad trick to always listen to keyboard

    if (!bIsBusy)
    {
      int CHcol = MazeView.getCharCol() - 1;
      int CHrow = MazeView.getCharRow() - 1;
      if(MoveKeys.getValue()=="Arrows"){
          if (keyEvent.getCode() == KeyCode.UP)
      {
          VM.MoveChar(CHrow - 1, CHcol);
      } else if (keyEvent.getCode() == KeyCode.DOWN)
      {
          VM.MoveChar(CHrow + 1, CHcol);
      } else if (keyEvent.getCode() == KeyCode.LEFT)
      {
          VM.MoveChar(CHrow, CHcol - 1);
      } else if (keyEvent.getCode() == KeyCode.RIGHT)
      {
          VM.MoveChar(CHrow, CHcol + 1);
      }}
     else if(MoveKeys.getValue()=="A_W_S_D"){
          if (keyEvent.getCode() == KeyCode.W)
          {
              VM.MoveChar(CHrow - 1, CHcol);
          } else if (keyEvent.getCode() == KeyCode.S)
          {
              VM.MoveChar(CHrow + 1, CHcol);
          } else if (keyEvent.getCode() == KeyCode.A)
          {
              VM.MoveChar(CHrow, CHcol - 1);
          } else if (keyEvent.getCode() == KeyCode.D)
          {
              VM.MoveChar(CHrow, CHcol + 1);
          }}
      else if(MoveKeys.getValue()=="4_8_5_6"){
          if (keyEvent.getCode() == KeyCode.NUMPAD8)
          {
              VM.MoveChar(CHrow - 1, CHcol);
          } else if (keyEvent.getCode() == KeyCode.NUMPAD5)
          {
              VM.MoveChar(CHrow + 1, CHcol);
          } else if (keyEvent.getCode() == KeyCode.NUMPAD4)
          {
              VM.MoveChar(CHrow, CHcol - 1);
          } else if (keyEvent.getCode() == KeyCode.NUMPAD6)
          {
              VM.MoveChar(CHrow, CHcol + 1);
          }}

    }
    keyEvent.consume();
  }


    /**
     * this func is responsible to catch notifications from the View Model and to act
     * @param o the Observable that notifies
     * @param arg an object represents the event
     */
  @Override
  public void update(Observable o, Object arg) {
    if (o == VM)
    {
      if (arg.toString().compareTo("newMaze") == 0)
      {
        //VM.status.set("The maze is ready, may the force be with you");
        MazeView.setcCol(VM.CharCol);
        MazeView.setcRow(VM.CharRow);
        MazeView.setEcol(VM.getEndCol() + 1);
        MazeView.setErow(VM.getEndRow() + 1);
        MazeView.setMaze(VM.getMaze());
        btn_getMaze.setDisable(false);
        btn_getSolution.setDisable(false);
        bWin=false;
      } else if (arg.toString().compareTo("newSolution") == 0)
      {
        bIsBusy = true;
        MazeView.setSolution(VM.getSolution());
        btn_getSolution.setDisable(false);
        btn_getMaze.setDisable(false);
        bIsBusy = false;
        VM.setStatus("Follow Yoda gaudiness, but be aware yoda will leave after you move");
      } else if (arg.toString().compareTo("MoveChar") == 0)
      {
        MazeView.setcCol(VM.CharCol);
        MazeView.setcRow(VM.CharRow);
        MazeView.reDraw();
      } else if (arg.toString().compareTo("AlertBedArgs") == 0)
      {
        AlertBox a = new AlertBox();
        a.display("Wrong Parameters", "You have entered wrong numbers, please try again");
        btn_getMaze.setDisable(false);
      } else if (arg.toString().compareTo("Win") == 0)
      {
        bWin = true;
        MazeView.victoryDraw();
        btn_getSolution.setDisable(true);

      }

    }
  }

    /**
     * this fuc link the View Model to the View.
     * @param viewModel
     */
  public void setViewModel(MyViewModel viewModel) {
    this.VM = viewModel;
    VM.Col.bind(txt_Col.textProperty());
    VM.Row.bind(txt_Row.textProperty());
    this.status.textProperty().bind(VM.status);

  }


    /**
     * that func allow as to adjest the size automatic
     * @param scene
     */
  public void setResizeEvent(Scene scene) {

    scene.widthProperty().addListener(new ChangeListener<Number>()
    {
      @Override
      public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth,
          Number newSceneWidth)
      {
        MazeView.setWidth(mainWin.getWidth());
        if (!bWin)
        {
          MazeView.reDraw();
        }
        else
        {
          MazeView.victoryDraw();
        }


      }
    });
    scene.heightProperty().addListener(new ChangeListener<Number>()
    {
      @Override
      public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight,
          Number newSceneHeight)
      {

        MazeView.setHeight(mainWin.getHeight());
        if (!bWin)
        {
          MazeView.reDraw();
        }
        else
        {
          MazeView.victoryDraw();
        }
      }
    });

  }

    /**
     * this func open a maze file
     */
  public void openFunc() {
    FileChooser FC = new FileChooser();
    FC.setTitle("Open file");
    FC.setInitialDirectory(new File("./resorces/SavedMazes"));
    File F = FC.showOpenDialog(null);
    if (F != null)
    {
      VM.openMazeF(F);
    }
  }

    /**
     * this func saves the current maze
     */
  public void saveFunc() {
    FileChooser FC = new FileChooser();
    FC.setTitle("Save file");
    FC.setInitialDirectory(new File("./resorces/SavedMazes"));

    File F = FC.showSaveDialog(null);
    if (F != null)
    {
      VM.saveMazeF(F);
    }
  }


  public void properties()
  {
    PropertiesDisplay p = new PropertiesDisplay();
    p.propertiesDisplay();
  }


  public void startDraw() {
    MazeView.startDraw();
    btn_getSolution.setDisable(true);
  }

  public void helpDisplay()
  {
    PropertiesDisplay p = new PropertiesDisplay();
    p.textDisplay("./resorces/Help.txt");
  }
    public void aboutDisplay()
    {
        PropertiesDisplay p = new PropertiesDisplay();
        p.textDisplay("./resorces/About.txt");
    }
  public void ShutDown(){
    Main.closeProgram();
  }


}
