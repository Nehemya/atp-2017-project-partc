package View;

import static java.lang.Thread.sleep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * this class extends canvas and its allows us to show in 2D our maze and to move on him as well
 **/
public class MazeView extends Canvas
{

  private int[][] maze;
  private int[][] solution;
  private StringProperty wallFileName, victoryFileName, playerFileName, goalFileName, startFileName, yodaFileName;
  private int cCol, cRow, Ecol, Erow;
  private MediaPlayer MPB,MPV;
  private Media backgroundS,Victory;
  public MazeView() {
    wallFileName = new SimpleStringProperty();
    victoryFileName = new SimpleStringProperty();
    playerFileName = new SimpleStringProperty();
    goalFileName = new SimpleStringProperty();
    startFileName = new SimpleStringProperty();
    yodaFileName = new SimpleStringProperty();
    cCol = 0;
    cRow = 0;
    backgroundS=new Media(new File("./resorces/BackgroundSound.mp3").toURI().toString());
    Victory=new Media(new File("./resorces/VictorySound.mp3").toURI().toString());
    MPB=new MediaPlayer(backgroundS);
    MPV=new MediaPlayer(Victory);
    setBackgroundS();
  }
  /********************some getters setters***********************************/
    public void setcCol(int cCol) { this.cCol = cCol + 1; }
    public void setcRow(int cRow) { this.cRow = cRow + 1; }
    public void setEcol(int ecol) { Ecol = ecol; }
    public void setErow(int erow) { Erow = erow;}
    public int getCharCol() { return cCol; }
    public int getCharRow() { return cRow; }
  /***************************************************************************/

  public void setSolution(int[][] solution) {
    this.solution = solution;
    drawSol();
  }

    /**getter for the maze mat
     * @return int[][] mat
     */
    public int[][] getMaze() {  return maze; }

    /**
     * this func display the solution clearly on the maze
     */
  private void drawSol() {

    double H = getHeight();
    double W = getWidth();
    double h = H * 0.8 / maze.length;
    double w = W * 0.8 / maze[0].length;

    GraphicsContext GC = getGraphicsContext2D();

    Image yoda = null;
    try
    {
      yoda = new Image(new FileInputStream(getYodaFileName()));
    } catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }


    for (int i = 0; i < solution.length; i++)
    {
      try
      {
        sleep(50);
      } catch (InterruptedException e)
      {
        e.printStackTrace();
      }
      GC.drawImage(yoda,(solution[i][1] + 1) * w, (solution[i][0] + 1) * h, w, h);
    }
  }

    /**
     *this func sets the new maze and then displays it
     * @param otherMaze new mat int[][]
     */
  public void setMaze(int[][] otherMaze) {
    this.maze = new int[otherMaze.length + 2][otherMaze[0].length + 2];
    for (int i = 0; i < this.maze.length; i++)
    {
      this.maze[i][0] = 1;
      this.maze[i][this.maze[0].length - 1] = 1;

    }
    for (int i = 0; i < this.maze[0].length; i++)
    {
      this.maze[0][i] = 1;
      this.maze[this.maze.length - 1][i] = 1;
    }
    for (int i = 0; i < otherMaze.length; i++)
    {
      for (int j = 0; j < otherMaze[0].length; j++)
      {
        this.maze[i + 1][j + 1] = otherMaze[i][j];
      }
    }
    reDraw();
  }

    /**
     * this func displays the maze and the character on it
     */
  public void reDraw() {
    if (maze != null)
    {
      setBackgroundS();

      double H = getHeight();
      double W = getWidth();
      double h = H * 0.8 / maze.length;
      double w = W * 0.8 / maze[0].length;

      GraphicsContext GC = getGraphicsContext2D();// drawing tool
      Image wall = null, player = null, end = null;
      try
      {
        wall = new Image(new FileInputStream(getWallFileName()));
        player = new Image(new FileInputStream(getPlayerFileName()));
        end = new Image(new FileInputStream(getGoalFileName()));
      } catch (FileNotFoundException e)
      {
        e.printStackTrace();
      }
      GC.clearRect(0, 0, W, H);//clear all befor anather drew
      for (int i = 0; i < maze.length; i++)
      {
        for (int j = 0; j < maze[0].length; j++)
        {
          if (wall == null)
          {
            if (maze[i][j] == 1)
            {
              GC.fillRect((double) j * w, (double) i * h, w, h);
            }
          }//draw walls
          else
          {
            if (maze[i][j] == 1)
            {
              GC.drawImage(wall, (double) j * w, (double) i * h, w, h);
            }

          }

        }

        //GC.fillRect((double) Ecol * w, (double) Erow * h, w, h);
        //GC.setFill(Color.RED);
        GC.drawImage(end, (double) Ecol * w, (double) Erow * h, w, h);
        //GC.fillOval(cCol * w, cRow * h, w, h);
        //GC.setFill(Color.BLACK);
        GC.drawImage(player,cCol * w, cRow * h, w, h );
      }
    }
  }


    /**
     * display the victory sign
     */
  public void victoryDraw()  {
    setVictoryS();
    GraphicsContext graphicsContext = getGraphicsContext2D();

    double dPaneWidth = getWidth();
    double dPaneHeight = getHeight();

    Image Victory = null;
    try
    {
      Victory = new Image(new FileInputStream(getVictoryFileName()));
      graphicsContext.clearRect(0, 0, dPaneWidth, dPaneHeight);
      graphicsContext.drawImage(Victory, 0, 0, dPaneWidth * 0.8, dPaneHeight * 0.8);
    } catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }


  }

    /**
     * displays the start imege
     */
  public void startDraw() {
    GraphicsContext graphicsContext = getGraphicsContext2D();
    double dWidthToDraw = getWidth();
    double dHieghtToDraw = getHeight();

    Image start = null;
    try
    {
      start = new Image(new FileInputStream(getStartFileName()));
      graphicsContext.clearRect(0,0, dWidthToDraw, dHieghtToDraw);
      graphicsContext.drawImage(start, 0, 0, dWidthToDraw * 0.8, dHieghtToDraw * 0.8);

    } catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }

  }

    /**
     * sets the background sound for the game
     */
  public void setBackgroundS(){
    MPV.stop();
    MPB.setVolume(0.1);
    MPB.setAutoPlay(true);
    MPB.setOnEndOfMedia(()->MPB.seek(Duration.ZERO));
    MPB.play();
  }

    /**
     * sets the victory sound once you win
     */
  public void setVictoryS(){
    MPB.stop();

    MPV.setVolume(0.1);
    MPV.setAutoPlay(true);
    MPV.setOnEndOfMedia(()->MPV.seek(Duration.ZERO));
    MPV.play();
  }

  /**************properties******************/

  public String getPlayerFileName() {
    return playerFileName.get();
  }

  public StringProperty playerFileNameProperty() {
    return playerFileName;
  }

  public void setPlayerFileName(String playerFileName) {
    this.playerFileName.set(playerFileName);
  }

  public String getGoalFileName() {
    return goalFileName.get();
  }

  public StringProperty goalFileNameProperty() {
    return goalFileName;
  }

  public void setGoalFileName(String goalFileName) {
    this.goalFileName.set(goalFileName);
  }

  public String getStartFileName() {
    return startFileName.get();
  }

  public StringProperty startFileNameProperty() {
    return startFileName;
  }

  public void setStartFileName(String startFileName) {
    this.startFileName.set(startFileName);
  }

  public String getYodaFileName() {
    return yodaFileName.get();
  }

  public StringProperty yodaFileNameProperty() {
    return yodaFileName;
  }

  public void setYodaFileName(String yodaFileName) { this.yodaFileName.set(yodaFileName);}

  public String getWallFileName() { return wallFileName.get();  }

  public void setWallFileName(String wallFileName) { this.wallFileName.set(wallFileName);  }

  public void setVictoryFileName(String victoryFileName) {this.victoryFileName.set(victoryFileName);}
  public String getVictoryFileName() {

        return victoryFileName.get();
    }
  public StringProperty victoryFileNameProperty() { return victoryFileName; }
}