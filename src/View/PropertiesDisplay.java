package View;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * this class allows us to display our properties properly to the user
 */
public class PropertiesDisplay
{

    /**
     * displays our properties
     */
  public  void propertiesDisplay()
  {
    Stage window = new Stage();

    window.initModality(Modality.APPLICATION_MODAL);
    window.setTitle("Properties");
    window.setMinHeight(100);
    window.setMinWidth(250);

    Pane pane = new Pane();

    Scene scene = new Scene(pane);
    scene.getStylesheets().add(getClass().getResource("ViewStyle.css").toExternalForm());
    Properties properties = new Properties();
    InputStream inputStream = null;

    try
    {
      inputStream = new FileInputStream("config.properties");
      properties.load(inputStream);
      properties.setProperty("mazeGenAlg", "Prim");


      String sConfig = "";
      Enumeration<?> e = properties.propertyNames();
      while (e.hasMoreElements())
      {
        String key = (String) e.nextElement();
        String value = properties.getProperty(key);
        sConfig += "Key: " + key + ", Value: " + value + "\n";
      }

      Label label = new Label(sConfig);
      pane.getChildren().addAll(label);
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    window.setScene(scene);
    window.showAndWait();
  }

    /**
     * this allows us to diplay som information givven
     * @param Path for the file that holds the data
     */
  public void textDisplay(String Path)
  {
    Stage window = new Stage();
    window.initModality(Modality.APPLICATION_MODAL);
    window.setTitle("Help");
    Pane pane = new Pane();
    Scene scene = new Scene(pane);
    scene.getStylesheets().add(getClass().getResource("ViewStyle.css").toExternalForm());

    StringBuffer fileData = new StringBuffer();
    try
    {
      BufferedReader reader = new BufferedReader(new FileReader(Path));
      char[] buf = new char[1024];
      int numRead = 0;
      while((numRead = reader.read(buf)) != -1)
      {
        String readData = String.valueOf(buf, 0, numRead);
        fileData.append(readData);
      }
      reader.close();
    } catch (IOException e)
    {
      e.printStackTrace();
    }

    Label label = new Label(fileData.toString());
    pane.getChildren().addAll(label);
    window.setScene(scene);
    window.showAndWait();


  }



}
