package Model;

import java.io.File;

/**
 * this interface defines the minimal functions needed from the Model layer
 */
public interface IModel {
     void GenMaze(int row,int col);
     int[][] getCurMaze();
     void SolveMaze();
     int[][] getMazeSolution();

    void MoveChar(int row, int col);
    int getCharRow();
    int getCharCol();

    void ShutDown();
    int getEndCol();
    int getEndRow();

    void openMazeF(File f);

    void saveMazeF(File f);
}
