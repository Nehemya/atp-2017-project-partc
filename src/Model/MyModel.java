package Model;

import IO.MyCompressorOutputStream;
import IO.MyDecompressorInputStream;
import Server.ServerStrategyGenerateMaze;
import Server.ServerStrategySolveSearchProblem;
import algorithms.mazeGenerators.Maze;
import Server.*;
import Client.*;
import algorithms.mazeGenerators.Position;
import algorithms.search.AState;
import algorithms.search.Solution;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl.ThreadStateMap.Byte0.runnable;

/**
 * the Model layer
 */
public class MyModel extends Observable implements IModel {
    private Maze curMaze;
    private int[][] mazeArray;
    private Solution mazeSolution;
    private int CharRow;
    private int CharCol, EndRow, EndCol;
    private ExecutorService threadPool= Executors.newCachedThreadPool();
    private Server mazeGeneratingServer,solveSearchProblemServer;

    /**
     * initialize the model and turn on the servers
     */
    public MyModel() {
        setUpServer();

    }

    /**
     * creates the 2 servers one to generates a maze and another to solve it
     */
    private void setUpServer() {
        mazeGeneratingServer = new Server(5400, 1000, new ServerStrategyGenerateMaze());
        solveSearchProblemServer = new Server(5401, 1000, new ServerStrategySolveSearchProblem());
        solveSearchProblemServer.start();
        mazeGeneratingServer.start();
    }



    /****************   Generate maze     *************************************************/
    /**
     * set up in a new thread to take the mission
     * @param row
     * @param col
     */
    public void GenMaze(int row,int col){
        threadPool.execute(()-> genRequest( row, col));
    }

    /**
     * getter for the cur maze as int mat
     * @return int[][] mat
     */
    public int[][] getCurMaze() {
         return curMaze.getMaze();
    }

    /**
     * getter for End point col
     * @return
     */
    @Override
    public int getEndCol() {
        return EndCol;
    }
    /**
     * getter for End point col
     * @return
     */
    @Override
    public int getEndRow() {
        return EndRow;
    }

    /**
     * set up a request for a new maze to send for the server and notify whan its ready to use
     * @param row
     * @param col
     */
    private void genRequest(int row, int col) {// need to see if all requests are answer
        try {
            Client client = new Client(InetAddress.getLocalHost(), 5400, new IClientStrategy() {
                @Override
                public void clientStrategy(InputStream inFromServer, OutputStream outToServer) {
                    try {
                        ObjectOutputStream toServer = new ObjectOutputStream(outToServer);
                        ObjectInputStream fromServer = new ObjectInputStream(inFromServer);
                        toServer.flush();
                        int[] mazeDimensions = new int[]{row, col};
                        toServer.writeObject(mazeDimensions); //send maze dimensions to server
                        toServer.flush();
                        byte[] compressedMaze = (byte[]) fromServer.readObject(); //read generated maze (compressed with MyCompressor) from server
                        InputStream is = new MyDecompressorInputStream(new ByteArrayInputStream(compressedMaze));
                        byte[] decompressedMaze = new byte[100000 /*CHANGE SIZE ACCORDING TO YOU MAZE SIZE*/]; //allocating byte[] for the decompressed maze -
                        is.read(decompressedMaze); //Fill decompressedMaze with bytes
                        curMaze = new Maze(decompressedMaze);
                        Position start = curMaze.getStart();
                        Position end = curMaze.getEnd();
                        CharCol = start.getCol();
                        CharRow = start.getRow();
                        EndCol = end.getCol();
                        EndRow = end.getRow();
                        mazeArray = curMaze.getMaze();
                        setChanged();
                        notifyObservers("newMaze");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            client.communicateWithServer();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }// make new maze and notify
    /**************************************************************************************/
    /*********************    Solve cur maze       ****************************************/
    public void SolveMaze(){
        threadPool.execute(()->solRequest());
    }

    public int[][] getMazeSolution() {
        ArrayList<AState> mazeSolutionSteps = mazeSolution.getSolutionPath();
        int[][] simpleSol=new int[mazeSolutionSteps.size()][2];
        for (int i = 0; i < mazeSolutionSteps.size(); i++) {
            String[] pars=mazeSolutionSteps.get(i).toString().split(",");
            simpleSol[i][0]=Integer.parseInt(pars[0].substring(1));
            simpleSol[i][1]=Integer.parseInt(pars[1].substring(0,pars[1].length()-1));//need to check if its true

        }
        return simpleSol;
    }

    private void solRequest() {
        try {
            Client client = new Client(InetAddress.getLocalHost(), 5401, new IClientStrategy() {
                @Override
                public void clientStrategy(InputStream inFromServer, OutputStream outToServer) {
                    try {
                        ObjectOutputStream toServer = new ObjectOutputStream(outToServer);
                        ObjectInputStream fromServer = new ObjectInputStream(inFromServer);
                        toServer.flush();
                        toServer.writeObject(curMaze); //send maze to server
                        toServer.flush();
                        mazeSolution = (Solution) fromServer.readObject(); //read generated maze (compressed with MyCompressor) from server
                        setChanged();
                        notifyObservers("newSolution");///notify all
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            client.communicateWithServer();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
   /**************************************************************************************/
   /*********************    move character      ****************************************/
    /*
    * this func moves the character if the move is possible
     */
    @Override
    public void MoveChar(int row, int col){ threadPool.execute(()->{

        if(row>=0&&col>=0&&row<mazeArray.length&&col<mazeArray[0].length){
            if((row+1==CharRow||row-1==CharRow||row==CharRow)&&(col+1==CharCol||col-1==CharCol||col==CharCol)){
                if(mazeArray[row][col]!=1){
                CharCol=col;
                CharRow=row;

                curMaze.setStart(new Position(row,col));
                if (row == EndRow && col == EndCol)
                {
                    setChanged();
                    notifyObservers("Win");
                }
                else
                {
                    setChanged();
                    notifyObservers("MoveChar");
                }

            }
            }
        }

    });}

    /**getter for char  row
     * @return int
     */
    public int getCharRow() {
        return CharRow;
    }

    /**
     * getter for the char col
     * @return int
     */
    public int getCharCol() {
        return CharCol;
    }
    /*********************************** ShutDown****************************************/

    /*
    *this func shut down the servers and the thread pool
     */
    @Override
    public void ShutDown() {
        solveSearchProblemServer.stop();
        mazeGeneratingServer.stop();
        threadPool.shutdown();
    }
    /****************************Files***************************/
    /*
    *this func load maze from a given mazfile
    * and notifys for it
     */
    @Override
    public void openMazeF(File f) {
        byte savedMazeBytes[] = new byte[0];
        try {
            //read maze from file
            InputStream in = new MyDecompressorInputStream(new FileInputStream(f));
            savedMazeBytes = new byte[1000000];
            in.read(savedMazeBytes);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        curMaze = new Maze(savedMazeBytes);
        Position start = curMaze.getStart();
        Position end = curMaze.getEnd();
        CharCol = start.getCol();
        CharRow = start.getRow();
        EndCol = end.getCol();
        EndRow = end.getRow();
        mazeArray = curMaze.getMaze();
        setChanged();
        notifyObservers("newMaze");
    }

    /**
     * this func saves the current maze in the file
     * @param f
     */
    @Override
    public void saveMazeF(File f) {
        try {
            // save maze to a file
            OutputStream out = new MyCompressorOutputStream(new FileOutputStream(f.toString()));
            out.write(curMaze.toByteArray());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
