package ViewModel;

import Model.IModel;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * this is our View Model layer
 * its roll is to conect and monitor between the View and the Model layers
 *
 */
public class MyViewModel extends Observable implements Observer
{

  public StringProperty Col, Row, status;
  public int CharCol, CharRow;
  private IModel Model;

  /**
   * conect the VM to the Model
   * @param model
   */
  public MyViewModel(IModel model) {
    Model = model;
    Col = new SimpleStringProperty("0");
    Row = new SimpleStringProperty("0");
    status = new SimpleStringProperty("Please create a new maze, if you need help go to the help menu");
  }

  /**
   * call for the Model to move the char
   * @param row
   * @param col
   */
  public void MoveChar(int row, int col) {
    status.setValue("Trying to move");
    Model.MoveChar(row, col);
  }

  /**
   * calls the Model to generat a new maze in a specific size
   */
  public void GenMaze() {
    int r = 0, c = 0;
    try
    {
      r = Integer.parseInt(Row.get());
      c = Integer.parseInt(Col.get());

      if (r < 10 || c < 10 || r > 400 || c > 400)
      {
        throw new NumberFormatException();
      } else
      {
        Model.GenMaze(r, c);
      }
    } catch (NumberFormatException n)
    {
      setChanged();
      notifyObservers("AlertBedArgs");

    }

  }

  /**
   * calls the Model to generates a solution for the maze
   */
  public void GenSolution() {
    Model.SolveMaze();
  }

  /**
   * this method catch the notifies thrown by the model and act
   * @param o
   * @param arg
   */
  @Override
  public void update(Observable o, Object arg) {
    if (o == Model)
    {
      if (arg.toString().compareTo("newMaze") == 0)
      {
        setStatus("The maze is ready, may the force be with you");
        CharRow = Model.getCharRow();
        CharCol = Model.getCharCol();
        setChanged();
        notifyObservers(arg);
      } else if (arg.toString().compareTo("newSolution") == 0)
      {
        setStatus("Please wait until yoda finished his gaudiness");
        setChanged();
        notifyObservers(arg);
      } else if (arg.toString().compareTo("MoveChar") == 0)
      {
        setStatus("You may proceed");
        CharCol = Model.getCharCol();
        CharRow = Model.getCharRow();
        setChanged();
        notifyObservers("MoveChar");
      } else if (arg.toString().compareTo("Win") == 0)
      {
        setStatus("You won, The force is strong with you");
        setChanged();
        notifyObservers("Win");
      }
    }

  }

  /**
   * gets the maze after the model has finished
   * @return int[][] mat
   */
  public int[][] getMaze() {
    return Model.getCurMaze();
  }

  /**
   * gets the solution from the model when its finished
   * @return
   */
  public int[][] getSolution() {
    return Model.getMazeSolution();
  }

  /**
   * getter for the End goal col
   * @return int
   */
  public int getEndCol() {
    return Model.getEndCol();
  }

  /**
   * getter for the End goal row
   * @return int
   */
  public int getEndRow() {
    return Model.getEndRow();
  }

  /**
   * call the model to shut down
   */
  public void closeProgram() {
    Model.ShutDown();
  }

  /**
   * pass on the request to open a file to the model
   * @param f
   */
  public void openMazeF(File f) {
    status.setValue("Opening file " + f.getName());
    Model.openMazeF(f);
  }

  /**
   * pass on the request to save a file to the model
   * @param f
   */
  public void saveMazeF(File f) {
    status.setValue("Saving file" + f.getName());
    Model.saveMazeF(f);
  }

  /**
   * sets the status bar connected
   * @param s
   */
  public void setStatus(String s)
  {
    Platform.runLater(new Runnable(){
      @Override
      public void run() {
        status.set(s);
      }
    });
  }
}
